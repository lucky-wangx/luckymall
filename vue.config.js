// 为文件夹取别名 
//  特别注意 注意  注意 
//  取完别名后 要重启项目才会生效
module.exports = {
    configureWebpack: {
        resolve: {
            alias: { //配置别名,修改后需要重新编译才能生效
                'assets': '@/assets',
                'common': '@/common',
                'components': '@/components',
                'network': '@/network',
                'views': '@/views',
            }
        }
    }
}