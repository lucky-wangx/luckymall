import { request } from "./request";
// 定义一个专门获取首页数据的方法，然后在首页上直接调这个
// 方法 就可以拿到数据
// 在方法内部只想封装好的请求文件

// 请求轮播图和热门数据
export function getHomeMultidata() {
    return request({
        url: '/home/multidata'
    })
}

// 请求商品列表数据
export function getHomeGoods(type,page) {
    return request({
        url: '/home/data',
        params: {
            type,
            page
        }
    })
}
