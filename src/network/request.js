// 引入axios模块
import axios from 'axios'
export function request(config) {
    // 创建axios实例
    const instance = axios.create({
        baseURL: 'http://152.136.185.210:7878/api/m5',
        timeout: 50000
    })
    // axios拦截器
    // 请求拦截
    instance.interceptors.request.use(config => {
        // 请求动画可以放在这里 使用show方法 
        return config
    },err => {
        console.log(err);
    })

    // 响应拦截
    instance.interceptors.response.use(res => {
        return res.data
    },err => {
        console.log(err);
    })

    // 发送真正的网络请求
    return instance(config)
}

// 另一种简单的方法
// export function request(config,success,failure) {
//     const instance = axios.create({
//         baseURL: 'http://152.136.185.210:7878/api/m5',
//         timeout: 5000
//     })
//     instance(config)
//         .then(res => {
//             success(res)
//         })
//         .catch(err => {
//             failure(err)
//         })
// }